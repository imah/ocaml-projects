let rec remove x = function
		| [] -> []
		| t :: q -> if t = x then remove x q
					else t :: remove x q;;
let copy a =
	let b = Array.make_matrix 9 9 [] in
		for i = 0 to 8 do
			for j = 0 to 8 do
				b.(i).(j) <- a.(i).(j);
			done;
		done;
		b;;

let print1 s =
	for i = 0 to 8 do
		for j = 0 to 8 do
			Printf.printf "%d " (List.hd s.(i).(j));
		done;
		Printf.printf "\n";
	done;;

let init_full s =
	for i = 0 to 8 do
		for j = 0 to 8 do
			s.(i).(j) <- [1; 2; 3; 4; 5; 6; 7; 8; 9];
		done;
	done;;

let rec control s t xx yy x y =
	if xx = x && yy = y then (* skip yourself *) true else
		begin
			let l = remove t s.(x).(y)
			in let l2 = List.length l
				in let l1 = List.length s.(x).(y) in
						if l2 = 0 then false (* updating the grid would reduce to nothing *)
						else
							begin (* list becomes smaller *)
								if l1 <> l2 then s.(x).(y) <- l;
								true;
							end end;

and run_vert s t xx yy i j =
	match i with
		| 9 -> true
		| a when a = xx -> run_vert s t xx yy (a + 1) j
		| a -> (control s t xx yy a j) && (run_vert s t xx yy (a + 1) j)

and run_hori s t xx yy i j = match j with
		| 9 -> true
		| b when b = yy -> run_hori s t xx yy i (b + 1)
		| b -> (control s t xx yy i b) && (run_hori s t xx yy i (b + 1))

and square s t xx yy i j =
	let a = 3 * (xx / 3)
	and b = 3 * (yy / 3) in (* square top left *)
		match (i, j) with
			| (3, 0) -> true
			| (i', 2) -> control s t xx yy (a + i') (b + 2) && square s t xx yy (i' + 1) 0
			| (i', j') -> control s t xx yy (a + i') (b + j') && square s t xx yy i' (j' + 1)
;;

let viable s x y =
	let t = List.hd s.(x).(y) in
		(run_vert s t x y 0 y)
		&& (run_hori s t x y x 0)
		&& (square s t x y 0 0);;

let run_nocond s x y =
	let t = List.hd s.(x).(y) in
		ignore (run_vert s t x y 0 y);
		ignore (run_hori s t x y x 0);
		ignore (square s t x y 0 0);
;;

let prune s =
	for i = 0 to 8 do
		for j = 0 to 8 do
		(* if grid is solveable, do not check conditions *)
			if 1 = List.length s.(i).(j) then (run_nocond s i j);
		done
	done;;

let rec iter_hints s k = function
		| [] -> false
		| t :: q ->
					let (i, j) = (k / 9, k mod 9)
					and w = copy s in
						w.(i).(j) <- [t];
						
						(* t is viable + the grid is completeable with t *)
						if (viable w i j) && (fill_at w (k + 1)) then
							(print1 w; failwith "found !");
						iter_hints s k q

and
fill_at s = function (* finishable *)
		| 81 -> true
		| k -> (iter_hints s k s.(k / 9).(k mod 9)) && fill_at s (k + 1);;

let solve s =
	let s' = copy s in
		prune s'; prune s'; prune s'; (* some pruning at the start *)
		fill_at s' 0;;

let s2 = Array.make_matrix 9 9 [];;
init_full s2;;

s2.(0).(0) <- [4];
s2.(0).(4) <- [1];
s2.(1).(2) <- [1];
s2.(2).(1) <- [5];
s2.(2).(2) <- [7];
s2.(2).(3) <- [6];
s2.(2).(7) <- [1];
s2.(3).(6) <- [3];
s2.(3).(8) <- [9];
s2.(4).(0) <- [2];
s2.(4).(3) <- [7];
s2.(4).(4) <- [5];
s2.(5).(0) <- [6];
s2.(5).(1) <- [3];
s2.(5).(5) <- [9];
s2.(5).(7) <- [5];
s2.(6).(0) <- [9];
s2.(6).(1) <- [4];
s2.(7).(4) <- [4];
s2.(7).(6) <- [2];
s2.(7).(8) <- [7];
s2.(8).(2) <- [6];
s2.(8).(4) <- [2];
s2.(8).(6) <- [1];
s2;;

solve s2;;

(* sample grid
4 - - - 1 - - - -
- - 1 - - - - - -
- 5 7 6 - - - 1 -
- - - - - - 3 - 9
2 - - 7 5 - - - -
6 3 - - - 9 - 5 -
9 4 - - - - - - -
- - - - 4 - 2 - 7
- - 6 - 2 - 1 - -
*)
(* matching solution
4 6 9 8 1 7 5 2 3
8 2 1 4 3 5 9 7 6
3 5 7 6 9 2 4 1 8
7 1 5 2 6 4 3 8 9
2 9 8 7 5 3 6 4 1
6 3 4 1 8 9 7 5 2
9 4 2 3 7 1 8 6 5
1 8 3 5 4 6 2 9 7
5 7 6 9 2 8 1 3 4
*)