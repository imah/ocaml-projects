#use "topfind";;
#require "graphics";;
#load "graphics.cma";;
#load "unix.cma";;
open Graphics;;

let n = 80;;
let a = Array.make_matrix n n 0;;(* width, height *)
(* 0:dead cell;  >=1: alive cell, bright red to black *)
(* darker color means the cell was alive for longer ! *)

let init_random a =
	let h = Array.length a in
		let w = Array.length a.(0) in
			for i = 10 to h - 10 do
				for j = 10 to w - 10 do
					a.(i).(j) <- Random.int 2; (* pick in between 0 or 1 *)
				done
			done;;

let get a i j = (* in alive calculation: watch out for the bounds of the grid *)
	let h = Array.length a in
		let w = Array.length a.(0) in
			if i < 0 then 0
			else if j < 0 then 0
			else if i >= h then 0
			else if j >= w then 0
			else a.(i).(j);;

let alive_int a i j = Bool.to_int (get a i j >= 1);;

let alive a i j =
	let s = ref 0 in
		for x = - 1 to 1 do
			for y = - 1 to 1 do
				s := !s + alive_int a (i + x) (j + y)
			done
		done;
		!s - alive_int a i j;;(* do not count yourself *)

let step a =
	let h = Array.length a in
		let w = Array.length a.(0) in
			let b = Array.make_matrix h w 0 in (* temporary for calculations *)
				for i = 0 to h - 1 do
					for j = 0 to w - 1 do
						
						let life = alive a i j in
							if (a.(i).(j) >= 1) && (life >= 2) && (life <= 3) then
								b.(i).(j) <- min 255 (a.(i).(j) + 10) (* do no step over max color intensity *)
							else if (a.(i).(j) = 0) && (life = 3) then
								b.(i).(j) <- 1
							else
								b.(i).(j) <- 0
					done
				done;
				
				for i = 0 to h - 1 do
					for j = 0 to w - 1 do
						a.(i).(j) <- b.(i).(j)
					done
				done;;

let draw a =
	let h = Array.length a in
		let w = Array.length a.(0) in
			for i = 0 to h - 1 do
				for j = 0 to w - 1 do
					let x = a.(i).(j) in
						if x = 0 then
							set_color white
						else
							set_color (rgb (255 - x) 0 0);
						fill_rect (5 * i) (5 * j) 5 5;
				done
			done;;

let pressed () =
	(wait_next_event [Poll]).keypressed (* if no key pressed says false to stop immediately *)
	&& (wait_next_event [Key_pressed]).keypressed;;

open_graph "400x400";;
clear_graph ();
set_window_title "Life Hector";
init_random a;

while not (pressed ()) do
	draw a;
	step a;
	Unix.sleepf 0.01;
done;;

(*close_graph ();;*)